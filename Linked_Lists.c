#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

// Function to create a new node
struct Node* createNode(int number) {
   struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = number;
    newNode->next = NULL;
    return newNode;
}


// Function to print the linked list
void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}


// Function to append a new node at the end
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
    } else {
        struct Node *current = *head;
        while (current->next != NULL) {
            current = current->next;
        }
        current->next = newNode;
    }
}

// Function to prepend a new node at the beginning
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to delete a node by key
void deleteByKey(struct Node **head, int key) {
    struct Node *current = *head;
    struct Node *prev = NULL;
// Traverse the list to find the node with a given key
    while (current != NULL && current->number != key) {
        prev = current;
        current = current->next;
    }
// if the node with the key is not found.
    if (current == NULL) {
        printf("Key not found in the list.\n");
        return;
    }
// if the node with the key is found then update the pointers to remove the key from the list.
    if (prev == NULL) {
        *head = current->next;
    } else {
        prev->next = current->next;
    }

    free(current);
}

// Function to delete a node by value
void deleteByValue(struct Node **head, int value) {
    struct Node *current = *head;
    struct Node *prev = NULL;
// Traverse the list to find the node with a given value
    while (current != NULL && current->number != value) {
        prev = current;
        current = current->next;
    }
// if the node with the value is not found.
    if (current == NULL) {
        printf("Value not found in the list.\n");
        return;
    }
// if the node with the value is found then update the pointers to remove the value from the list.
    if (prev == NULL) {
        *head = current->next;
    } else {
        prev->next = current->next;
    }

    free(current);
}

// Function to insert a new node after a given key
void insertAfterKey(struct Node **head, int key, int value) {
    struct Node *current = *head;

    while (current != NULL && current->number != key) {
        current = current->next;
    }

    if (current == NULL) {
        printf("Key not found in the list.\n");
        return;
    }

    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;
}

// Function to insert a new node after a given value
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *current = *head;

    while (current != NULL && current->number != searchValue) {
        current = current->next;
    }

    if (current == NULL) {
        printf("Value not found in the list.\n");
        return;
    }

    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next = newNode;
}

int main() {
    struct Node *head = NULL;
    int choice, data, key, newValue;

    while (1) {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch(choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: ");
                scanf("%d", &key);
                deleteByKey(&head, key);
                break;
            case 5:
                printf("Enter value to delete: ");
                scanf("%d", &data);
                deleteByValue(&head, data);
                break;
            case 6:
                printf("Enter key after which to insert: ");
                scanf("%d", &key);
                printf("Enter new value to insert: ");
                scanf("%d", &newValue);
                insertAfterKey(&head, key, newValue);
                break;
            case 7:
                printf("Enter value after which to insert: ");
                scanf("%d", &data);
                printf("Enter new value to insert: ");
                scanf("%d", &newValue);
                insertAfterValue(&head, data, newValue);
                break;
            case 8:
                printf("Exiting program.\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}





